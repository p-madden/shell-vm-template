1. Change `project-name` in `vagrant-nsidc.yaml`
2. Edit `provision` to your liking
3. `vagrant nsidc up --env=<env>`
